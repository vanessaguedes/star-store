package se.accenture.assignment.store.controller;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.accenture.assignment.store.model.Product;


@Controller
public class HomeController {
    
    @RequestMapping("/home")
    public ModelAndView listProducts()
    {
            ArrayList<Product> products = new ArrayList<Product>();
            products.add(new Product("001", "Blue Cat", "5000", "A cool blue cat", "https://i.ytimg.com/vi/CGUriNVnKBg/hqdefault.jpg"));
            products.add(new Product("002", "Pink Cat", "6000", "A cool pink cat", "http://i.dailymail.co.uk/i/pix/2010/09/24/article-1314962-0B55EC2B000005DC-960_638x401.jpg"));
            products.add(new Product("003", "Purple Cat", "3000", "A cool purple cat", "http://orig07.deviantart.net/ca8a/f/2014/219/3/4/purple_cat_by_mrverdi-d7u5m47.jpg"));
            
            
            ModelAndView mv = new ModelAndView("home");
            mv.addObject("products", products);
            return mv;
    }

}
