# Star Store

This is a simple one page application that shows products.
It uses:

* Angular 4
* Typescript
* Spring Framework


## Installation

You must have Java, and IDE, and Nodejs installed.

Download this repository to your computer or clone it by using: `git clone`.

After cloning or downloading it, use your terminal and go to your repo.

`cd path/to/the/repo`

Run `mvn spring-boot:run`.

Open a new tab on the terminal and go the /client folder.

Then you must install all the npm packs to launch the page.

`npm install`

This last command will create a new folder in your repo, node_modules. It contains all the dependencies to the project view.

## Run the project

Use `ng serve` in your project folder to run the application. If you don't have the angular-cli installed, you can follow [these steps](https://github.com/angular/angular-cli#installation). e

To stop the server, just use `ctrl+C` in the terminal tab where you started npm.

**IMPORTANT 1:*** Make sure both apps are started (with mvn spring-boot:run in the server directory, and ng serve in the client directory) and navigate to http://localhost:4200.

**IMPORTANT 2:*** The angular view is in the second branch in this repo. Change to this branch before running the services.
